# build the application
FROM maven:3.6.0-jdk-8-alpine AS builder
COPY . .
RUN mvn clean package

FROM alpine:latest

COPY --from=builder target/*-fat.jar /krt.jar

RUN apk --update add --no-cache bash curl util-linux openjdk11

CMD ["/bin/sh"]
