package garden.bots.kotlinrt

import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import java.util.logging.Level
import java.util.logging.Logger

class MainVerticle : AbstractVerticle() {

  override fun stop(stopPromise: Promise<Void>) {
    stopPromise.complete()
  }

  override fun start(startPromise: Promise<Void>) {
    Logger.getLogger("io.vertx.core.impl.BlockedThreadChecker").setLevel(Level.OFF);

    val kompilo = Kompilo()
    val functionName = "main"
    val functionCode = System.getenv("kotlin") ?: """
      fun ${functionName}(): Unit {
        println("hello world")
      }
    """.trimIndent()

    val compiledFunction = kompilo.compileFunction(functionCode)

    compiledFunction
      .onFailure {
        println("😡 compilation error: ${it.message}")
        startPromise.fail(it.cause)
      }
      .onSuccess {
        startPromise.complete()
        // compilation is OK
        kompilo.invokeFunction(functionName)
          .onFailure {
            println("😡 execution error: ${it.message}")
          }
          .onSuccess {
            // execution is OK
            println("🙂 execution success ${it.toString()}") // return void
          }
      }
    vertx.close()

  }
}
