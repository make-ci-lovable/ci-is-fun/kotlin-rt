package garden.bots.kotlinrt

import io.vertx.ext.web.RoutingContext
import javax.script.Invocable
import javax.script.ScriptEngineManager

class Kompilo {

  private val engine = ScriptEngineManager().getEngineByExtension("kts")!!
  private val invoker = engine as Invocable

  /*
  fun compileFunction(functionCode: String, language: String) : Result<Any> {
    return try {
      Result.success(engine.eval(functionCode))
    } catch (exception: Exception) {
      Result.failure<Exception>(exception)
    }
  }
  */

  fun compileFunction(functionCode: String) : Result<Any> {
    return Result.runCatching {
      engine.eval(functionCode)
    }
  }

  fun invokeFunction(name: String?, params: Any) : Result<Any> {
    return Result.runCatching {
      invoker.invokeFunction(name, params)
    }
  }

  fun invokeFunction(name: String?, params: Any, context: RoutingContext) : Result<Any> {
    return Result.runCatching {
      invoker.invokeFunction(name, params, context)
    }
  }

  fun invokeFunction(name: String?) : Result<Any> {
    return Result.runCatching {
      invoker.invokeFunction(name)
    }
  }
}
